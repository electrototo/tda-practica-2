from aux_functions import aux_functions as aux

english_notes = [
    'c', 'd', 'e', 'f', 'g', 'a', 'b'
]

semitones = {'c': [0, 1], 'd': [-1, 0, 1], 'e': [0, -1], 'f': [0, 1], 'g': [-1, 0, 1], 'a': [-1, 0, 1], 'b': [-1, 0]}

spanish_notes = [
    'do', 're', 'mi', 'fa', 'sol', 'la', 'si'
]

# Creates a frequency table for the notes and all the octaves
# the resulting table must match the table from:
# http://pages.mtu.edu/~suits/notefreqs.html
for octave in range(0, 9):
    for en, sp in zip(english_notes, spanish_notes):
        for semitone in semitones[en]:
            en_freq = aux.get_note_freq(en, semitone, octave)
            sp_freq = aux.get_note_freq(sp, semitone, octave)

            sem = ''
            if semitone == -1:
                sem = 'b'
            elif semitone == 1:
                sem = '#'

            print(f'Note: {en:3s}, octave: {octave}, semitone: {sem}, Freq: {en_freq:.2f}')
            print(f'Note: {sp:3s}, ocatve: {octave}, semitone: {sem}, Freq: {sp_freq:.2f}')
            print()
