from aux_functions import aux_functions as aux
import numpy as np
import pyaudio
import csv

csv_file = 'csv_files/spanish.csv'
notes = []

fs = 44100

p = pyaudio.PyAudio()
stream = p.open(format=pyaudio.paFloat32, channels=1, rate=fs, output=True)

with open(csv_file) as f:
    reader = csv.reader(f)
    header = next(reader)

    for row in reader:
        note = row[0]
        mod = int(row[1])
        octave = int(row[2])
        duration = float(row[3])

        freq = aux.get_note_freq(note, mod, octave)
        print('freq:', freq)

        sound = 0.5 * np.sin(2 * np.pi * np.arange(0, duration, 1 / fs) * freq).astype(np.float32)
        stream.write(sound)

stream.close()
p.terminate()
