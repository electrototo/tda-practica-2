# Cristobal Liendo Infate
# Práctica 2: Taller de Desarrollo de Aplicaciones

import os
import csv
import queue
import pyaudio
import argparse
import numpy as np

import aux_functions.aux_functions as aux


class MusicPlayer():
    def __init__(self, filename):
        self.filename = filename
        self.notes_queue = queue.Queue()
        self.fs = 44100

        self.save_repeat = False

        self.player = pyaudio.PyAudio()
        self.stream = self.player.open(
            format=pyaudio.paFloat32, channels=1, rate=self.fs, output=True)

        if not os.path.isfile(filename):
            print('The file {} was not found.'.format(filename))
            raise SystemExit

    def __del__(self):
        self.stream.close()
        self.player.terminate()

    def play(self):
        with open(self.filename) as f:
            reader = csv.reader(f)
            header = next(reader)

            if not aux.check_header(header):
                print('Bad CSV header.')
                raise SystemExit

            for row in reader:
                if not aux.validate(row):
                    pass

                data = aux.clean(row)
                self.play_note(data)

                if self.save_repeat:
                    self.notes_queue.put(data)

                if data['flag'] == 's':
                    self.save_repeat = True
                    self.notes_queue.put(data)

                elif data['flag'] == 'p':
                    self.play_queue()

    def play_queue(self):
        while not self.notes_queue.empty():
            data = self.notes_queue.get()
            self.play_note(data)

        self.save_repeat = False

    def play_note(self, data):
        t = np.arange(0, data['duration'], 1 / self.fs)
        sound_wave = 0.7 * np.sin(2 * np.pi * t * data['freq'])

        self.stream.write(sound_wave.astype(np.float32))


if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument('-f', '--file', required=True, help='Path to the csv file')

    args = vars(ap.parse_args())

    player = MusicPlayer(args['file'])
    player.play()
