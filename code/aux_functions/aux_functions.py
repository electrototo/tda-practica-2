def get_freq(n):
    """Implements the formula for the requency of the notes.

    http://pages.mtu.edu/~suits/NoteFreqCalcs.html

    Attibutes
    ---------
    n : int
        The number of half steps away from the fixed note you are

    Returns
    -------
    float
        The frequency of one fixed note
    """
    return 440 * pow(2, (n / 12))


def get_note_freq(note, mod, octave):
    """
    Returns the frequency of the specified note and octave with its
    corresponding modifier.

    Attributes
    ----------
    note : int
        The identifier of the note. Valid values are:
            A : 0
            B : 2
            C : 3
            D : 5
            E : 7
            F : 8
            G : 10

    modifier : int
        The modifier for the specified note. Valid values are:
            flat : -1
            natural : 0
            sharp : 1

    octave : int
        The octave of the note. Valid values are between 0 and 8 inclusive

    Returns
    -------
    float
        The frequency of the specified note and its modifiers (ocatave, accident)
    """

    note = note_id(note)

    if octave > 4 or (octave == 4 and note <= 2):
        if note >= 3:
            n = note + mod + (octave - 5) * 12
        else:
            n = note + mod + (octave - 4) * 12

    else:
        if note >= 3:
            n = -(12 - note - mod + (4 - octave) * 12)
        else:
            n = -(12 - note - mod + (3 - octave) * 12)

    return get_freq(n)


def note_id(note):
    """Get the note's identifier

    Get the notes identfier for later use in get_note_freq. The note can
    be written in english notation (A, B, C...) or in spanish notation
    (do, re, mi ...)

    Parameters
    ----------
    note : String
        The note to get the identifier

    Returns
    -------
    int
        The note's identifier
    """

    english_values = {
        'a': 0,
        'b': 2,
        'c': 3,
        'd': 5,
        'e': 7,
        'f': 8,
        'g': 10
    }

    spanish_values = {
        'la': 0,
        'si': 2,
        'do': 3,
        're': 5,
        'mi': 7,
        'fa': 8,
        'sol': 10
    }

    index = -1

    if note in english_values:
        index = english_values[note]

    elif note in spanish_values:
        index = spanish_values[note]

    return index


def check_header(header):
    """Check the header's syntax

    Check if the header of the csv file is well formed

    Parameters
    ----------
    header : array like
        CSV header to be checked

    Returns
    -------
    boolean
        True if well formed, False otherwise

    """

    params = ['nombre_nota', 'accidente', 'numero_octava', 'duracion', 'bloque']
    well_formed = True

    for (hd, pd) in zip(header, params):
        if hd != pd:
            well_formed = False
            break

    return well_formed


def validate(row):
    """Validates the CSV row data

    Validates the CSV: It must contain the elements that were defined
    in the header, in the same order

    Parameters
    ----------
    row : array like
        CSV row data

    Returns
    -------
    boolean
        True if valid, False otherwise
    """

    v = False
    if len(row) == 5:
        note = note_id(row[0])

        try:
            mod = int(row[1])
            octave = int(row[2])
            duration = float(row[3])

        except ValueError:
            mod = -42
            octave = -42
            duration = -42

        flag = row[4]

        v = (note != -1) and (mod in range(-1, 2)) and (octave in range(0, 9)) and (duration > 0) and (flag in ['s', 'x', 'p'])

    return v


def clean(row):
    """Cleans the CSV row data

    Cleans the CSV row data in orther to obtain the note's data

    Parameters
    ----------
    row : array like
        Validated CSV row data

    Returns
    -------
    dictionary
        A dictonary containing the frequency, duration and repeat flag
    """

    note, mod, octave, dur, flag = row

    mod = int(mod)
    octave = int(octave)
    dur = float(dur)

    freq = get_note_freq(note, mod, octave)
    pkt = {'duration': dur, 'freq': freq, 'flag': flag}

    return pkt
