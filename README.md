# Práctica II
Reproducción de una canción en formato CSV.

Cristóbal Liendo Infante.

[cristobal@liendo.net](mailto:cristobal@liendo.net)
***
#### Descripción
En este repositorio se podrán encontrar todos los archivos que consisten en la práctica número II. Dentro de `code` se encuentran tres directorios y un archivo:
1. `aux_functions`
2. `csv_files`
3. `test`
4. `music_player.py`

Dentro de `aux_functions` se encuentra un módulo de python, el cual se encarga de simplificar el código principal, `music _player.py`. Los métodos que se pueden encontrar en éste son para obtener la frecuencia de la nota, independientemente del idioma en el que se encuentre (inglés o español) y métodos de limpieza y verificación de datos.

`csv_files` contiene algunos archivos CSV de prueba para poder ser utilizados por `music_player.py`:
1. *english.csv*: contiene notas en inglés.
2. *spanish.csv*: contiene notas en español.
3. *repetition.csv*: contiene unas notas de prueba para demostrar la funcionalidad del la repetición de éstas.
4. *badiniere.csv*: traducción de badiniere de bach a un archivo CSV.
5. *g_minor.csv*: traducción de g_minor de bach a un archivo CSV.

Dentro de los archivos de *badiniere.csv* y *repetition.csv* se puede observar la funcionalidad por bloques, para la repetición de notas.
Los archivos *english.csv* y *spanish.csv* al ser reproducidos deben sonar iguales, puesto que son los mismos pero con los nombres de las notas en otro idioma.

`tests` es la carpeta que contiene las pruebas para la obtención de las frecuencias a partir del nombre de la nota y sus modificadores (número de octava y alteración).

#### Repetición por bloques
Para que no sea necesario repetir los bloques de notas dentro del archivo CSV, y así reducir su tamaño, fue neceario la construcción de un mecanismo para que éste se encargara de repetir las notas necesarias.
El funcionamiento es el siguiente:
1. La primera nota que se encuentre después de los dos puntos de repetición tendrá en el campo *bloque*, dentro del archivo CSV, la bandera s.
2. Las demás notas, sin importar si se encuentran dentro de un bloque de repetición, deberán tener una *x* en la columna *bloque* del archivo CSV.
3. La última nota que se encuentre antes de los dos puntos de término de repetición deberá tener la bandera *p* la columna de *bloque* del archivo CSV.

Cuando el programa encuentra una nota con la bandera *s*, guarda ésta y todas las notas siguientes dentro de una cola. Cuando se encuentra la bandera *p*, manda a llamar una función para reproducir todas la notas dentro de la cola.

#### Invocación del programa
Para poder reproducir un archivo CSV es necesario ejecutar el programa de la siguiente forma:
> `python music_player.py -f FILENAME`

Donde *FILENAME* es el archivo CSV que contiene las notas.

#### Entorno
Dentro de `requirements.txt` se encuentran las librerías no nativas de python para desarrollar la práctica.

#### Pruebas
Si se desea ejecutar las pruebas dentro de `test` es necesario usar el siguiente comando:
> `python -m unittest discover`

Será necesario ejecutar el comando dentro de la carpeta `code`.

#### Extra
La extracción de notas de ambos archivos, *badiniere.csv* y *g_minor.csv* fue hecha mediante reconocimiento de imágenes con OpenCV. El proceso no fue 100% autonómo puesto que tenía que especificar la línea de *e4*. Y *entrenar* al algoritmo, fuera de eso el proceso fue autónomo.
A continuación se muestra una imagen obtenida dentro del proceso de reconocimiento de notas:

![alt text](https://gitlab.com/electrototo/tda-practica-2/raw/b5310036165863ea0733ca42afe2a128f6bc7183/images/detected.png "Notas")